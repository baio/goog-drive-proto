'use strict';

function initAuth() {
  // auth stuff
  return gapi.auth2
    .init({
      client_id:
        // client id from Google Cloud Platform project
        // https://console.cloud.google.com/apis/dashboard
        // TODO: change clientId
        '651426349758-obb5uhstojsarhd4fpn04bbmatnao0pb.apps.googleusercontent.com',
      scope:
        // auth is obrigatory to see all files
        'https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive'
    })
    .then(res => {
      return res.currentUser;
    })
    .then(res => {
      var currentUser = res.get();
      if (!currentUser.isSignedIn()) {
        var inst = gapi.auth2.getAuthInstance();
        return inst.signIn();
      } else {
        return currentUser;
      }
    });
}

angular
  .module('myApp.view1', ['ngRoute', 'scalioGoogleDrive'])

  .config([
    '$routeProvider',
    function($routeProvider) {
      $routeProvider.when('/view1', {
        templateUrl: 'view1/view1.html',
        controller: 'View1Ctrl'
      });
    }
  ])
  .config([
    'googleDriveProvider',
    function(googleDriveProvider) {
      googleDriveProvider.init({
        clientId:
          '651426349758-obb5uhstojsarhd4fpn04bbmatnao0pb.apps.googleusercontent.com',
        parentFolderId: '1peiIHEQZ-Yw-2zBiiZVTATUiBfjKM4I8'
      });
    }
  ])
  .controller('View1Ctrl', [
    '$scope',
    'googleDrive',
    function($scope, googleDrive) {
      var fields =
        'files(contentHints,fullFileExtension,id,modifiedTime,name,originalFilename,thumbnailLink)';

      new Promise(x => setTimeout(x, 2000))
        .then(() => googleDrive.getFiles(fields))
        .then(res => {
          $scope.files = res;
          $scope.$digest();
        });
    }
  ]);
