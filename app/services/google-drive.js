'use strict';

function GoogleDrive(gapi, $http, config) {
  this.initAuth = function() {
    return gapi.auth2
      .init({
        client_id:
          // client id from Google Cloud Platform project
          config.clientId,
        scope:
          // auth is obrigatory to see all files
          'https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive'
      })
      .then(res => {
        return res.currentUser;
      })
      .then(res => {
        var currentUser = res.get();
        if (!currentUser.isSignedIn()) {
          var inst = gapi.auth2.getAuthInstance();
          return inst.signIn();
        } else {
          return currentUser;
        }
      });
  };

  this.getFiles = function(fields) {
    return this.initAuth().then(user => {
      var resp = user.getAuthResponse();
      return $http
        .get('https://www.googleapis.com/drive/v3/files', {
          params: {
            // here parent folder id for search (consider use TeamDriver, teamDriveId)
            q: "'" + config.parentFolderId + "' in parents",
            fields
          },
          headers: {
            authorization: resp.token_type + ' ' + resp.access_token
          }
        })
        .then(res => res.data.files);
    });
  };
}

// Declare app level module which depends on views, and components
angular
  .module('scalioGoogleDrive', [])
  .provider('googleDrive', function GoogleDriveProvider() {
    var config;

    this.init = function(val) {
      config = val;
    };

    this.$get = [
      '$http',
      function($http) {
        if (!gapi) {
          throw new Error('GAPI is not initialized');
        }
        return new GoogleDrive(gapi, $http, config);
      }
    ];
  });
