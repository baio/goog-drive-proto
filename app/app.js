'use strict';


// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/view1'});

}]);

/*
angular.module('ngm.ngDrive')
	.provider('OauthService', ngDrive.Config)
	.config(function (OauthServiceProvider) {
        // 651426349758-obb5uhstojsarhd4fpn04bbmatnao0pb.apps.googleusercontent.com
        // huwT_exs-7i53IBibDXMO4JT
		OauthServiceProvider.setScopes('https://www.googleapis.com/auth/drive.file');
		OauthServiceProvider.setClientID('651426349758-obb5uhstojsarhd4fpn04bbmatnao0pb.apps.googleusercontent.com');
		OauthServiceProvider.setTokenRefreshPolicy(ngDrive.TokenRefreshPolicy.ON_DEMAND);
    });
*/